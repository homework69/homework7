"""
Простым называется число, которое делится нацело только на единицу и само себя. Число 1 не считается простым.
Напишите программу, которая находит все простые числа в заданном промежутке, выводит их на экран, а затем по
требованию пользователя выводит их сумму либо произведение.
"""


def get_basic_numbers(beg, end):
    """
    Повертає список простих чисел із заданого проміжку за допомогою алгоритму "Решето Ератосфена"

    початок проміжку: param beg: int
    кінець проміжку :param end: int
    :return: list
    """
    beg = max(beg, 2)
    list_basic = []
    if beg > end:
        return list_basic

    list_all = list(range(end + 1))
    list_all[1] = 0

    i = 2
    while i <= end:
        if list_all[i] != 0:
            if i >= beg:
                list_basic.append(list_all[i])
            if i * i <= end:
                for j in range(i, end + 1, i):
                    list_all[j] = 0

        i += 1

    return list_basic


# print(get_basic_numbers(-100, -10))
# print(get_basic_numbers(-100, 0))
# print(get_basic_numbers(-100, 10))
# print(get_basic_numbers(10, 30))
# print(get_basic_numbers(2, 100))

interval_beg = int(input("Введіть початок інтервалу: "))
interval_end = int(input("Введіть початок інтервалу: "))

ll_basic = get_basic_numbers(interval_beg, interval_end)
print(f"Прості числа з проміжку {interval_beg} - {interval_end}: \n{ll_basic}")

print("Доступні операції: \n"
      "Сума чисел: 1 \n"
      "Добуток чисел: 2")

operation = input("Виберіть операцію: ")
if operation == '1':
    print(f"Сума чисел: {sum(ll_basic)}")
elif operation == '2':
    mult = 1
    for i in ll_basic:
        mult *= i
    print(f"Добуток чисел: {mult}")
else:
    print("Операція не визначена!")
