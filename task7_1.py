"""
Создайте список и введите его значения. Найдите наибольший и наименьший элемент списка,
а также сумму и среднее арифметическое его значений.
"""


def del_non_number(ll):
    result = []
    for val in ll:
        if isinstance(val, float) or isinstance(val, int):
            result.append(val)
    return result


def sum_list_number(ll):
    list_number = del_non_number(ll)
    return sum(list_number)


def min_list_number(ll):
    list_number = del_non_number(ll)
    if len(list_number) != 0:
        ord_ll = sorted(list_number)
        return ord_ll[0]


def max_list_number(ll):
    list_number = del_non_number(ll)
    if len(list_number) != 0:
        ord_ll = sorted(list_number, reverse=True)
        return ord_ll[0]


def average_list_number(ll):
    list_number = del_non_number(ll)
    suma = sum(list_number)
    count = len(list_number)
    return 0 if count == 0 else suma / count


my_list = ['a', 'qwerty', 5, 6.0, -1, 100, 0.5, 0.333, -2.5]
# my_list = [1, 2, 3, 4, 5]
# my_list = ['a', 'qwerty']
# my_list = []


print(f"Максимальний числовий елемент: {max_list_number(my_list)}")
print(f"Мінімальний числовий елемент: {min_list_number(my_list)}")
print(f"Сума числових елементів: {sum_list_number(my_list)}")
print(f"Середнє арифметичне: {average_list_number(my_list)}")
