"""
Создайте список, введите количество его элементов и сами значения, выведите эти значения на экран в обратном порядке.
"""
elem_count = int(input("Input count of elements: "))
elem_list = []
if elem_count > 0:
    for i in range(elem_count):
        elem = input(f"Input {i} element: ")
        elem_list.append(elem)

print("Elemens in reversed ordynary: ")
for i in elem_list[::-1]:
    if i == elem_list[0]:
        print(i)
    else:
        print(i, end=', ')
